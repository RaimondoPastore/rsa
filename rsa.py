from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_OAEP
import binascii
import argparse
from datetime import datetime


def get_str_timestamp(form="%Y.%b.%d-%H.%M.%S"):
    return datetime.now().strftime(form)


def write_file(name, content):
    f = open(name, "w")
    f.write(content)
    f.close()


def generate_rsa_keys(size=3072):
    keyPair = RSA.generate(size)
    pubKey = keyPair.publickey()

    pubKeyPEM = pubKey.exportKey()
    privKeyPEM = keyPair.exportKey()

    name_file = get_str_timestamp()

    write_file(name_file + "_pub_key.txt", pubKeyPEM.decode('ascii'))
    write_file(name_file + "_private_key.txt", privKeyPEM.decode('ascii'))

    print(f"Public key:\n {pubKeyPEM.decode('ascii')}")
    print(f"Private key:\n {privKeyPEM.decode('ascii')}")


def encrypt(key, message):
    rsa_key = RSA.importKey(key)
    binary_message = message.encode("ascii")
    encryptor = PKCS1_OAEP.new(rsa_key)
    encrypted = encryptor.encrypt(binary_message)
    return binascii.hexlify(encrypted)


def decrypt(key, encrypted_message):
    rsa_key = RSA.importKey(key)
    binary = binascii.unhexlify(encrypted_message)
    decryptor = PKCS1_OAEP.new(rsa_key)
    return decryptor.decrypt(binary)


def switch(mode, key_path, message):
    s = {0: lambda: generate_rsa_keys(),
         1: lambda: encrypt(open(path_key).read(), message),
         2: lambda: decrypt(open(path_key).read(), message)}

    if mode not in s:
        raise ValueError

    return s[mode]


parser = argparse.ArgumentParser(description='Tool for RSA encr/decr')

# Input Arguments
parser.add_argument('--mode', '-m', action='store', type=int, required=True,
                    help="Values: 0 = generate new pair keys, 1 = encrypt, 2 = decrypt")
parser.add_argument('--path_key', '-k', action='store', type=str, help="Path of the file where the key is stored")
parser.add_argument('--message', '-msg', action='store', type=str, help="Message to encode or decode")


args = parser.parse_args()
mode, path_key, message = args.mode, args.path_key, args.message

try:
    fun = switch(mode, path_key, message)
    value = fun()
    if value:
        print(value.decode("utf-8"))
except ValueError:
    print("--mode (-m) can be only 0, 1 or 2")
